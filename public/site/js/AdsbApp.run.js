﻿/**
 * Configuración del run
 * 
 * @author demorales13@gmail.com
 * @since 3-dic-2016
 *
 */

(function () {
    'use strict';

    angular.module('AdsbApp')
           .run(runBlock);

    runBlock.$inject = ['$log', '$stateParams', '$rootScope', '$cookieStore', '$location', '$state'];


    function runBlock($log, $stateParams, $rootScope, $cookieStore, $location, $state) {

        $rootScope.location = $location;
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.$on('$stateChangeSuccess', function () {
            // scroll view to top
            $("html, body").animate({ scrollTop: 0 }, 200);
        });

        // Mostrar por defecto el menú lateral colapsado
        $cookieStore.put('sideNavCollapsed', true);
        $rootScope.sideMenuAct = true;
    };
})();
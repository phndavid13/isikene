﻿/**
 * Definición de enrutamientos
 *
 * @author Nelson David Padilla H
 * @since 23-02-2017
 *
 */

(function () {
    "use strict";

    angular.module("AdsbApp")
           .config(registerRoutes);

    registerRoutes.$inject = ["$stateProvider", "$urlRouterProvider"];

    function registerRoutes($stateProvider, $urlRouterProvider) {

        $urlRouterProvider
            .when("/", "home")
            .otherwise("home");

        $stateProvider
            // Home
            .state("home", {
                page_title: "Danda",
                url: "/home",
                templateUrl: "views/home.html"                
            })
            // Test
            .state("test", {
                page_title: "Danda - Autoevalucación",
                url: "/test/:id",
                templateUrl: "views/test/test.html"                
            })
            // Feedback One
            .state("feedback", {
                page_title: "Danda - Autoevalucación",
                url: "/feedback/:id?d1",
                templateUrl: "views/test/feedback.html",
                controller:"FeedBackController"
            })
            // Buy
            .state("program", {
                page_title: "Danda - Autoevalucación",
                url: "/program",
                templateUrl: "views/test/program.html"                
            })


    };
}());

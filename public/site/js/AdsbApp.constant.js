﻿/**
 * Definición de constantes
 *
 * @author demorales13@gmail.com
 * @since 3-dic-2016
 *
 */

(function () {
	'use strict';

    angular.module('AdsbApp')
	       .constant('BaseUri', {
	  	        protocol: 'http',
	  	        host: 'danda.com.co',
           		path: '/api/',
	  	        get url() {
	  	            return this.protocol + '://' + this.host + this.path; }
           })		  
})();

﻿/**
 * Servicio para interactual con los mapas de google
 *
 * @author demorales13@gmail.com
 * @since 17-dic-2016
 *
 */

(function () {
    "use strict";

    angular
        .module('AdsbApp')
        .service('GoogleMapService', GoogleMapService);

    GoogleMapService.$inject = [];

    function GoogleMapService() {

        var drawPath = function (path) {

            var collection = [];

            for (var i = 1; i < path.length; i++) {
                if (path[i].msgtype == 3 && path[i].latitude != null && path[i].longitude != null && path[i - 1].latitude != null && path[i - 1].longitude != null) {
                    var step = [path[i - 1], path[i]];
                    var color = altitudeColor(path[i].altitude);
                    var draw = drawStep(step, color);
                    collection.push(draw);
                }
            }

            return collection;
        }

        var drawStep = function (step, color) {
            return {
                path: step,
                stroke: {
                    color: color,
                    weight: 3
                },
                geodesic: true,
                visible: true,
                icons: [{
                    icon: {
                        path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW
                    },
                    offset: '25px',
                    repeat: '50px'
                }]
            };
        }

        var drawMarkers = function (type, markers) {
            var collection = [];

            for (var i = 0; i < markers.length; i++) {
                collection.push(createMarker(i, type, markers[i]));
            }

            return collection;
        }

        var createMarker = function (i, type, point) {
            var ret = {
                id: i,
                latitude: point.latitude,
                longitude: point.longitude,
                alert: point.alert,
                icon: iconType(type)
            };

            return ret;
        }

        var iconType = function (type) {

            if (type == "gndspd") {
                return 'content/images/speed.png';
            } else if (type == "vspd") {
                return 'content/images/caution.png';
            } else if (type == "emerg") {
                return 'content/images/flag.png';
            } else if (type == "sqwk") {
                return 'content/images/radiotower.png';
            }

        }

        var altitudeColor = function (altitude) {

            if (altitude >= 0 && altitude <= 499)
                return '#FF0000';
            else if (altitude >= 500 && altitude <= 999)
                return '#FF6600';
            else if (altitude >= 1000 && altitude <= 1999)
                return '#CC9900';
            else if (altitude >= 2000 && altitude <= 2999)
                return '#FFCC00';
            else if (altitude >= 3000 && altitude <= 4999)
                return '#00CC00';
            else if (altitude >= 5000 && altitude <= 7499)
                return '#0033FF';
            else if (altitude >= 7500 && altitude <= 10000)
                return '#9900CC';
        }

        var fitMap = function (map, polylines) {
            var bounds = new google.maps.LatLngBounds();

            var firtsStep = new google.maps.LatLng(polylines[0].path[0].latitude,
                polylines[0].path[0].longitude);
            var lastStep = new google.maps.LatLng(polylines[polylines.length - 1].path[0].latitude,
                polylines[polylines.length - 1].path[0].longitude);
            
            bounds.extend(firtsStep);
            bounds.extend(lastStep);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        }

        return {
            drawPath: drawPath,
            drawMarkers: drawMarkers,
            fitMap: fitMap
        }
    }
} ());

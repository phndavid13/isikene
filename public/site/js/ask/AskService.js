/**
 * Service for business logic
 *
 * @author Nelson David Padilla H. phndavid@gmail.com
 * @since 20-01-2017
 *
 */
(function(){
  'use strict';
  angular.module('AdsbApp')
  .service('AskService',AskService)
  AskService.$inject = ['RestService'];
  function AskService(RestService){
    var getQuestions = function () {
        return RestService.get('read_questions.php')
            .then(function (response) {
                return response.data.questions;
            });
    }
    var getFeedBack = function(){
      return RestService.get('feedback.json')
          .then(function (response) {
              return response.data;
          });
    }
    var postCreatePlayer = function(email, name, company,  result){
      return RestService.post('create_player.php','email='+email+'&name='+name+'&company='+company+'&result='+result)
          .then(function (response) {
              return response.data;
          });
    }
    return {
        getQuestions: getQuestions,
        getFeedBack: getFeedBack,
        postCreatePlayer: postCreatePlayer
    }
  }
}());

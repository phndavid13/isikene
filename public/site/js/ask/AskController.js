/**
 * Controller for questions and answers
 *
 * @author Nelson David Padilla H. phndavid@gmail.com
 * @since 20-01-2017
 *
 */
 (function(){
  'use strict';
  angular.module('AdsbApp')
    .controller("AskController", AskController);
    AskController.$inject =['$scope','$state', '$stateParams', '$location','LocalStorageService','AskService'];
    function AskController($scope,$state,$stateParams, $location, LocalStorageService, AskService){
      const SCORE = "score";
      const PROGRESS = "progress";
      const PROGRESS_CO = "progress_co";
      const PROGRESS_LE = "progress_le";
      const PROGRESS_AS = "progress_as";
      const PROGRESS_AO = "progress_ao";
      const PROGRESS_M = "progress_m";
      const NUM = "num";
      const NUM1 = "num1";
      const NUM2 = "num2";
      const NUM3 = "num3";
      const NUM4 = "num4";
      const NUM5 = "num5";
      const CATEGORY_1 = "Contexto";
      const CATEGORY_2 = "Liderazgo";
      const CATEGORY_3 = "Soporte";
      const CATEGORY_4 = "Operacion";
      const CATEGORY_5 = "Mejora";            
      var questions = [];
      var score = 0;
      $scope.totalQuestion;
      $scope.data_user = false;
      $scope.progress_co = 0;
      $scope.progress_le = 0;
      $scope.progress_as = 0;
      $scope.progress_ao = 0;
      $scope.progress_m = 0;
      $scope.progress = 0;
      $scope.question = {
          "id": "0",
          "type": "Example1",
          "question": "Questions"
        };
      $scope.credentials = {
          name: "",
          email: ""
      }

      $scope.answer = function(id,porcent){                
        var num = $stateParams.id;          
        addProgress(questions[num-1].type);
        num++;        
        $location.path('test/'+num);
        $location.replace();        
        var question = questions[num-1];
        $scope.question = question;
        $scope.progress = (num/$scope.totalQuestion)*100;        
        score += porcent;        
        LocalStorageService.add(NUM, num);
        LocalStorageService.add(PROGRESS, $scope.progress);
        LocalStorageService.add(SCORE, score);        
      }
      var num1=0,num2=0,num3=0,num4=0,num5=0;
      var addProgress = function(type){
        if(type == CATEGORY_1){          
          num1++;
          $scope.progress_co = (num1/19)*100;
          LocalStorageService.add(PROGRESS_CO, $scope.progress_co);          
          LocalStorageService.add(NUM1, num1);
        }else if(type == CATEGORY_2){          
          num2++;
          $scope.progress_le = (num2/5)*100;
          LocalStorageService.add(PROGRESS_LE, $scope.progress_le);          
          LocalStorageService.add(NUM2, num2);
        }else if(type == CATEGORY_3){          
          num3++;
          $scope.progress_as = (num3/15)*100;
          LocalStorageService.add(PROGRESS_AS, $scope.progress_as);          
          num3 = LocalStorageService.add(NUM3,num3);
        }else if(type == CATEGORY_4){
          num4++;
          $scope.progress_ao = (num4/14)*100;
          LocalStorageService.add(PROGRESS_AO, $scope.progress_ao);      
          num4 = LocalStorageService.add(NUM4,num4);
        }else if(type == CATEGORY_5){
          num5++;
          $scope.progress_m = (num5/7)*100;
          LocalStorageService.add(PROGRESS_M, $scope.progress_m);        
          num5 = LocalStorageService.add(NUM5,num5);
        }
      }  
      $scope.showResult = function(form){
        if (form.$valid) {
          $scope.data_user = true;
          var email = $scope.credentials.email;
          var name = $scope.credentials.name;
          var company = $scope.credentials.company;
          var result = "Por Terminar";
          console.log('email',email)
          console.log('name',name)
          console.log('company', company)
          AskService.postCreatePlayer(email,name, company, result).then(function(response){
            if(response.status == "OK"){
              getFeedBack();
              LocalStorageService.add(PROGRESS, 0);
              LocalStorageService.add(NUM, 1);
            }else{
              console.log(response);
            }
          },function(error){
            console.log(error);
          })

        }
      }
      var getFeedBack = function(){
        AskService.getFeedBack().then(function(response){
          score = score/$scope.totalQuestion;
          console.log("Score",score);
          if(score >= 0 && score < 25){
            $state.go('feedback', {id: 1});
          }else if(score >= 25 && score < 50){            
            $state.go('feedback', {id: 2});
          }else if(score >= 50 && score < 75){            
            $state.go('feedback', {id: 3});
          }else if(score >=75){            
            $state.go('feedback', {id: 4});
          }
        }, function (error) {
            console.log(error);
        })
      }
      var initQuestion = function(){                     
        $scope.progress = LocalStorageService.get(PROGRESS);
        progress();
        console.log("progress", $scope.progress);
        if($scope.progress != null && $scope.progress != 0){          
          var num = LocalStorageService.get(NUM);
          var question = questions[num-1];          
          $scope.question = question;
          $location.path('test/'+num);
          $location.replace(); 
        }else{
          console.log("INIT")
          var num = 1;     
          LocalStorageService.add(NUM,num);          
          var question = questions[num-1];          
          $scope.question = question;          
          initNum();
          initProgress();
          $location.path('test/'+num);
          $location.replace(); 
        }
      }
      var progress = function(){
        num1 = LocalStorageService.get(NUM1) || 1;
        console.log("PROGRESS NUM1", num1) || 1;
        num2 = LocalStorageService.get(NUM2) || 1;
        num3 = LocalStorageService.get(NUM3) || 1;
        num4 = LocalStorageService.get(NUM4) || 1;
        num5 = LocalStorageService.get(NUM5) || 1;
        $scope.progress_co = LocalStorageService.get(PROGRESS_CO);
        $scope.progress_le = LocalStorageService.get(PROGRESS_LE);
        $scope.progress_as = LocalStorageService.get(PROGRESS_AS);
        $scope.progress_ao = LocalStorageService.get(PROGRESS_AO);
        $scope.progress_m = LocalStorageService.get(PROGRESS_M);
      }
      var initProgress = function(){
        $scope.progress_co = LocalStorageService.add(PROGRESS_CO,0);
        $scope.progress_le = LocalStorageService.add(PROGRESS_LE,0);
        $scope.progress_as = LocalStorageService.add(PROGRESS_AS,0);
        $scope.progress_ao = LocalStorageService.add(PROGRESS_AO,0);
        $scope.progress_m = LocalStorageService.add(PROGRESS_M,0);
      }
      var initNum = function(){        
        num1 = LocalStorageService.add(NUM1,1);
        num2 = LocalStorageService.add(NUM2,1);
        num3 = LocalStorageService.add(NUM3,1);
        num4 = LocalStorageService.add(NUM4,1);
        num5 = LocalStorageService.add(NUM5,1);        
      }
      let loadQuestions = function(){
        AskService.getQuestions().then(function (response) {
                questions = response;          
                $scope.totalQuestion = questions.length;
                $scope.question = questions[0];
                initQuestion();
              },function (error) {
                  console.log(error);
              });
      }
      var inicialize = function () {
          loadQuestions();        
      }();
    }
}());

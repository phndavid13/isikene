/**
 * Service for business logic
 *
 * @author Nelson David Padilla H. phndavid@gmail.com
 * @since 20-01-2017
 *
 */
(function(){
  'use strict';
  angular.module('AdsbApp')
  .service('FeedBackService',FeedBackService)
  FeedBackService.$inject = ['RestService'];
  function FeedBackService(RestService){
    var getWhere = function () {
        return {          
          msg_one: "La empresa cuenta con un importante avance de implementación y cumplimiento de requisitos del sistema de gestión 25%, debemos identificar cuáles son las brechas y empezar a trabajar.",
          msg_two: "Las noticias son muy buenas un 50% de avance de implementación es una buena noticia. Debemos saber cuáles son las brechas y desarrollar un plan de acción que esperamos pronto muestre sus resultados, como hasta ahora lo ha hecho  la empresa",
          msg_three: "Muy buenas noticias, el resultado del esfuerzo y el trabajo nos sitúan en un 75% de avance, nos hace falta poco, una lista de chequeo de lo que tenemos por mejorar y cerrar para el sistema de gestión implementado. ",
          msg_four: "Un excelente resultado, 100% de implementación del sistema de gestión. ",
        }
    }
    var getMissing = function(){
      return {
        msg_one:"Lo más importante por implementar cuando contamos con porcentaje de avance y hemos iniciado con la implementación es tener claro el plan de acción y aquí te lo ayudamos a construir.",
        msg_two:"Es importante cuando tienes un % de avance importante, revisar lo que nos está haciendo falta, que tipo de desarrollo y acompañamiento requiere el personal y que ajustes debemos hacer para avanzar en el camino adecuado, la planeación y la gestión de lo que hace falta es de vital importancia para asegurar el éxito del sistema de gestión. ",
        msg_three:"Prepararse para que el sistema de gestión se consolide como una herramienta de gestión y organización, y para las auditorías internas que están incluidas en nuestros paquetes de servicios",
        msg_four:"Falta demostrar mediante una auditoria presencial la conformidad del sistema de gestión, nosotros la programamos y la hacemos en la empresa, presentamos el informe con el objeto de identificar las posibles desviaciones o mejoras del sistema de gestión. ",
      }
    }
    var getTips = function(){
      return {
        msg_one:[
            {tip: "La socialización de los avances de implementación son importantes para todo el equipo.", icon:"nicono1.png"},
            {tip: "El equipo debe conocer los componentes actuales de los sistemas de gestión.",icon:"nicono2.png"},
            {tip: "Sobre la gestión documental se deben mantener los documentos, los registros y las evidencias, recuerda que pueden ser visuales, electrónicas o físicas, lo que sea más óptimo para cada uno de los procesos.",icon:"nicono3.png"}
          ],          
        msg_two:[
            {tip: "Cómo está la matriz de riesgo de la empresa?", icon:"micono1.png"},
            {tip: "Estamos trabajando sobre los objetivos, política del sistema de gestión?",icon:"micono2.png"},
            {tip: "Es importante la formación en sistema de gestión para los colaboradores, nosotros te decimos como.",icon:"micono3.png"}
          ],                
        msg_three:[
            {tip: "El personal, el cliente, la gestión, el clima deben estar en permanente evaluación y proceso de mejoramiento.",icon:"aicono1.png"},
            {tip: "La gestión documental debe ser una excelente herramienta de trabajo.", icon:"aicono2.png"},
            {tip: "El Ciclo PHVA aunque muy utilizado aplica para las tareas cotidianas y las más estratégicas no lo olvidemos.", icon:"aicono3.png"}         
        ],
        msg_four:[
            {tip: "Luego de las auditorías internas, preparar la gestión de los respectivos planes de acción.",icon:"vicono1.png"},
            {tip: "Del cierre del ciclo PHVA depende el avance y la consolidación del sistema de gestión.",icon:"vicono2.png"},
            {tip: "La validación con el cliente nunca se detiene.",icon:"vicono3.png"}
        ]        
      } 
    }
    var getHomes = function(){
      return {
        home1: [{icon1:"casa_naranja_1.jpg", icon2: "gris_2.png", icon3: "gris_3.png", icon4: "gris_4.png"}],
        home2: [{icon1:"casa_violeta_1.png", icon2: "casa_violeta_2.png", icon3: "gris_3.png", icon4: "gris_4.png"}],
        home3: [{icon1:"casa_azul_1.jpg", icon2: "casa_azul_2.jpg", icon3: "casa_azul_3.jpg", icon4: "gris_4.png"}],
        home4: [{icon1:"casa_verde_1.jpg", icon2: "casa_verde_2.jpg", icon3: "casa_verde_3.jpg", icon4: "casa_verde_4.jpg"}],
      }
    }
    return {
        getWhere: getWhere,
        getMissing: getMissing,
        getTips: getTips,
        getHomes: getHomes
    }
  }
}());

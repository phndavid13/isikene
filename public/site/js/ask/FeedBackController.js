/**
 * Controller for questions and answers
 *
 * @author Nelson David Padilla H. phndavid@gmail.com
 * @since 20-01-2017
 *
 */
 (function(){
  'use strict';
  angular.module('AdsbApp')
    .controller("FeedBackController", FeedBackController);
    FeedBackController.$inject =['$scope','$stateParams', '$location','LocalStorageService','FeedBackService'];
    function FeedBackController($scope,$stateParams, $location, LocalStorageService, FeedBackService){
      var wheres = FeedBackService.getWhere();
      var missing = FeedBackService.getMissing();
      var tips = FeedBackService.getTips();
      var homes =FeedBackService.getHomes();

      var colors = { 
        color1:[{in:"orange_in", out:"orange_out"}], 
        color2:[{in:"violet_in", out:"violet_out"}],
        color3:[{in:"blue_in", out:"blue_out"}],
        color4:[{in:"green_in", out:"green_out"}]
      }
      
      $scope.where = "";
      $scope.miss = "";
      $scope.colors = "";
      var loadWhere = function(type){      
        if(type == 1){
          $scope.where = wheres.msg_one;
        }else if(type == 2){
          $scope.where = wheres.msg_two;
        }else if(type == 3){
          $scope.where = wheres.msg_three;
        }else if(type == 4) {
          $scope.where = wheres.msg_four;
        }
      }

      var loadMissing = function(type){
        if(type == 1){
          $scope.miss = missing.msg_one;
        }else if(type == 2){
          $scope.miss = missing.msg_two;
        }else if(type == 3){
          $scope.miss = missing.msg_three;
        }else if(type == 4) {
          $scope.miss = missing.msg_four;
        }
      }

      var loadTips = function(type){
        if(type == 1){
          $scope.tip = tips.msg_one;
        }else if(type == 2){
          $scope.tip = tips.msg_two;
        }else if(type == 3){
          $scope.tip =  tips.msg_three;
        }else if(type == 4) {
          $scope.tip = tips.msg_four;
        } 
      }

      var loadColor = function(type){      
        if(type == 1){
          $scope.colors = colors.color1;
        }else if(type == 2){
          $scope.colors = colors.color2;
        }else if(type == 3){
          $scope.colors = colors.color3;
        }else if(type == 4) {
          $scope.colors = colors.color4;
        }
        console.log("colors", $scope.colors);
      }

      var loadHomes = function(type){      
        if(type == 1){
          $scope.home = homes.home1;
        }else if(type == 2){
          $scope.home = homes.home2;
        }else if(type == 3){
          $scope.home = homes.home3;
        }else if(type == 4) {
          $scope.home = homes.home4;
        }
        console.log("home", $scope.home);
      }

      var inicialize = function () {
        var type = $stateParams.id;        
        loadWhere(type);
        loadMissing(type);
        loadTips(type);
        loadColor(type);
        loadHomes(type);
      }();
    }
}());

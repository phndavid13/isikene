/**
 * Creaci�n del m�dulo principal del aplicativo
 * 
 * @author demorales13@gmail.com
 * @since 3-dic-2016
 *
 */

(function () {
    'use strict';

    angular.module("AdsbApp",
        ["ngTouch",
         "toastr",
         "ui.router",
         "ngCookies",
         "ngAnimate",
         "ui.bootstrap",
         "ngMessages",
         "ngMaterial",
         "ncy-angular-breadcrumb",
         "uiGmapgoogle-maps",
         "vAccordion",
         "angular-loading-bar"
        ]);
}());

/**
 * Definición de enrutamientos
 *
 * @author Nelson David Padilla H
 * @since 23-02-2017
 *
 */

(function () {
    "use strict";

    angular.module("AdsbApp")
           .config(registerRoutes);

    registerRoutes.$inject = ["$stateProvider", "$urlRouterProvider"];

    function registerRoutes($stateProvider, $urlRouterProvider) {

        $urlRouterProvider
            .when("/", "home")
            .otherwise("home");

        $stateProvider
            // Home
            .state("home", {
                page_title: "Danda",
                url: "/home",
                templateUrl: "views/home.html"                
            })
            // Test
            .state("test", {
                page_title: "Danda - Autoevalucación",
                url: "/test/:id",
                templateUrl: "views/test/test.html"                
            })
            // Feedback One
            .state("feedback", {
                page_title: "Danda - Autoevalucación",
                url: "/feedback/:id?d1",
                templateUrl: "views/test/feedback.html",
                controller:"FeedBackController"
            })
            // Buy
            .state("program", {
                page_title: "Danda - Autoevalucación",
                url: "/program",
                templateUrl: "views/test/program.html"                
            })


    };
}());

/**
 * Configuración del run
 * 
 * @author demorales13@gmail.com
 * @since 3-dic-2016
 *
 */

(function () {
    'use strict';

    angular.module('AdsbApp')
           .run(runBlock);

    runBlock.$inject = ['$log', '$stateParams', '$rootScope', '$cookieStore', '$location', '$state'];


    function runBlock($log, $stateParams, $rootScope, $cookieStore, $location, $state) {

        $rootScope.location = $location;
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.$on('$stateChangeSuccess', function () {
            // scroll view to top
            $("html, body").animate({ scrollTop: 0 }, 200);
        });

        // Mostrar por defecto el menú lateral colapsado
        $cookieStore.put('sideNavCollapsed', true);
        $rootScope.sideMenuAct = true;
    };
})();
/**
 * Controller for questions and answers
 *
 * @author Nelson David Padilla H. phndavid@gmail.com
 * @since 20-01-2017
 *
 */
 (function(){
  'use strict';
  angular.module('AdsbApp')
    .controller("AskController", AskController);
    AskController.$inject =['$scope','$state', '$stateParams', '$location','LocalStorageService','AskService'];
    function AskController($scope,$state,$stateParams, $location, LocalStorageService, AskService){
      const SCORE = "score";
      const PROGRESS = "progress";
      const PROGRESS_CO = "progress_co";
      const PROGRESS_LE = "progress_le";
      const PROGRESS_AS = "progress_as";
      const PROGRESS_AO = "progress_ao";
      const PROGRESS_M = "progress_m";
      const NUM = "num";
      const NUM1 = "num1";
      const NUM2 = "num2";
      const NUM3 = "num3";
      const NUM4 = "num4";
      const NUM5 = "num5";
      const CATEGORY_1 = "Contexto";
      const CATEGORY_2 = "Liderazgo";
      const CATEGORY_3 = "Soporte";
      const CATEGORY_4 = "Operacion";
      const CATEGORY_5 = "Mejora";            
      var questions = [];
      var score = 0;
      $scope.totalQuestion;
      $scope.data_user = false;
      $scope.progress_co = 0;
      $scope.progress_le = 0;
      $scope.progress_as = 0;
      $scope.progress_ao = 0;
      $scope.progress_m = 0;
      $scope.progress = 0;
      $scope.question = {
          "id": "0",
          "type": "Example1",
          "question": "Questions"
        };
      $scope.credentials = {
          name: "",
          email: ""
      }

      $scope.answer = function(id,porcent){                
        var num = $stateParams.id;          
        addProgress(questions[num-1].type);
        num++;        
        $location.path('test/'+num);
        $location.replace();        
        var question = questions[num-1];
        $scope.question = question;
        $scope.progress = (num/$scope.totalQuestion)*100;        
        score += porcent;        
        LocalStorageService.add(NUM, num);
        LocalStorageService.add(PROGRESS, $scope.progress);
        LocalStorageService.add(SCORE, score);        
      }
      var num1=0,num2=0,num3=0,num4=0,num5=0;
      var addProgress = function(type){
        if(type == CATEGORY_1){          
          num1++;
          $scope.progress_co = (num1/19)*100;
          LocalStorageService.add(PROGRESS_CO, $scope.progress_co);          
          LocalStorageService.add(NUM1, num1);
        }else if(type == CATEGORY_2){          
          num2++;
          $scope.progress_le = (num2/5)*100;
          LocalStorageService.add(PROGRESS_LE, $scope.progress_le);          
          LocalStorageService.add(NUM2, num2);
        }else if(type == CATEGORY_3){          
          num3++;
          $scope.progress_as = (num3/15)*100;
          LocalStorageService.add(PROGRESS_AS, $scope.progress_as);          
          num3 = LocalStorageService.add(NUM3,num3);
        }else if(type == CATEGORY_4){
          num4++;
          $scope.progress_ao = (num4/14)*100;
          LocalStorageService.add(PROGRESS_AO, $scope.progress_ao);      
          num4 = LocalStorageService.add(NUM4,num4);
        }else if(type == CATEGORY_5){
          num5++;
          $scope.progress_m = (num5/7)*100;
          LocalStorageService.add(PROGRESS_M, $scope.progress_m);        
          num5 = LocalStorageService.add(NUM5,num5);
        }
      }  
      $scope.showResult = function(form){
        if (form.$valid) {
          $scope.data_user = true;
          var email = $scope.credentials.email;
          var name = $scope.credentials.name;
          var company = $scope.credentials.company;
          var result = "Por Terminar";
          console.log('email',email)
          console.log('name',name)
          console.log('company', company)
          AskService.postCreatePlayer(email,name, company, result).then(function(response){
            if(response.status == "OK"){
              getFeedBack();
              LocalStorageService.add(PROGRESS, 0);
              LocalStorageService.add(NUM, 1);
            }else{
              console.log(response);
            }
          },function(error){
            console.log(error);
          })

        }
      }
      var getFeedBack = function(){
        AskService.getFeedBack().then(function(response){
          score = score/$scope.totalQuestion;
          console.log("Score",score);
          if(score >= 0 && score < 25){
            $state.go('feedback', {id: 1});
          }else if(score >= 25 && score < 50){            
            $state.go('feedback', {id: 2});
          }else if(score >= 50 && score < 75){            
            $state.go('feedback', {id: 3});
          }else if(score >=75){            
            $state.go('feedback', {id: 4});
          }
        }, function (error) {
            console.log(error);
        })
      }
      var initQuestion = function(){                     
        $scope.progress = LocalStorageService.get(PROGRESS);
        progress();
        console.log("progress", $scope.progress);
        if($scope.progress != null && $scope.progress != 0){          
          var num = LocalStorageService.get(NUM);
          var question = questions[num-1];          
          $scope.question = question;
          $location.path('test/'+num);
          $location.replace(); 
        }else{
          console.log("INIT")
          var num = 1;     
          LocalStorageService.add(NUM,num);          
          var question = questions[num-1];          
          $scope.question = question;          
          initNum();
          initProgress();
          $location.path('test/'+num);
          $location.replace(); 
        }
      }
      var progress = function(){
        num1 = LocalStorageService.get(NUM1) || 1;
        console.log("PROGRESS NUM1", num1) || 1;
        num2 = LocalStorageService.get(NUM2) || 1;
        num3 = LocalStorageService.get(NUM3) || 1;
        num4 = LocalStorageService.get(NUM4) || 1;
        num5 = LocalStorageService.get(NUM5) || 1;
        $scope.progress_co = LocalStorageService.get(PROGRESS_CO);
        $scope.progress_le = LocalStorageService.get(PROGRESS_LE);
        $scope.progress_as = LocalStorageService.get(PROGRESS_AS);
        $scope.progress_ao = LocalStorageService.get(PROGRESS_AO);
        $scope.progress_m = LocalStorageService.get(PROGRESS_M);
      }
      var initProgress = function(){
        $scope.progress_co = LocalStorageService.add(PROGRESS_CO,0);
        $scope.progress_le = LocalStorageService.add(PROGRESS_LE,0);
        $scope.progress_as = LocalStorageService.add(PROGRESS_AS,0);
        $scope.progress_ao = LocalStorageService.add(PROGRESS_AO,0);
        $scope.progress_m = LocalStorageService.add(PROGRESS_M,0);
      }
      var initNum = function(){        
        num1 = LocalStorageService.add(NUM1,1);
        num2 = LocalStorageService.add(NUM2,1);
        num3 = LocalStorageService.add(NUM3,1);
        num4 = LocalStorageService.add(NUM4,1);
        num5 = LocalStorageService.add(NUM5,1);        
      }
      let loadQuestions = function(){
        AskService.getQuestions().then(function (response) {
                questions = response;          
                $scope.totalQuestion = questions.length;
                $scope.question = questions[0];
                initQuestion();
              },function (error) {
                  console.log(error);
              });
      }
      var inicialize = function () {
          loadQuestions();        
      }();
    }
}());

/**
 * Service for business logic
 *
 * @author Nelson David Padilla H. phndavid@gmail.com
 * @since 20-01-2017
 *
 */
(function(){
  'use strict';
  angular.module('AdsbApp')
  .service('AskService',AskService)
  AskService.$inject = ['RestService'];
  function AskService(RestService){
    var getQuestions = function () {
        return RestService.get('read_questions.php')
            .then(function (response) {
                return response.data.questions;
            });
    }
    var getFeedBack = function(){
      return RestService.get('feedback.json')
          .then(function (response) {
              return response.data;
          });
    }
    var postCreatePlayer = function(email, name, company,  result){
      return RestService.post('create_player.php','email='+email+'&name='+name+'&company='+company+'&result='+result)
          .then(function (response) {
              return response.data;
          });
    }
    return {
        getQuestions: getQuestions,
        getFeedBack: getFeedBack,
        postCreatePlayer: postCreatePlayer
    }
  }
}());

/**
 * Controller for questions and answers
 *
 * @author Nelson David Padilla H. phndavid@gmail.com
 * @since 20-01-2017
 *
 */
 (function(){
  'use strict';
  angular.module('AdsbApp')
    .controller("FeedBackController", FeedBackController);
    FeedBackController.$inject =['$scope','$stateParams', '$location','LocalStorageService','FeedBackService'];
    function FeedBackController($scope,$stateParams, $location, LocalStorageService, FeedBackService){
      var wheres = FeedBackService.getWhere();
      var missing = FeedBackService.getMissing();
      var tips = FeedBackService.getTips();
      var homes =FeedBackService.getHomes();

      var colors = { 
        color1:[{in:"orange_in", out:"orange_out"}], 
        color2:[{in:"violet_in", out:"violet_out"}],
        color3:[{in:"blue_in", out:"blue_out"}],
        color4:[{in:"green_in", out:"green_out"}]
      }
      
      $scope.where = "";
      $scope.miss = "";
      $scope.colors = "";
      var loadWhere = function(type){      
        if(type == 1){
          $scope.where = wheres.msg_one;
        }else if(type == 2){
          $scope.where = wheres.msg_two;
        }else if(type == 3){
          $scope.where = wheres.msg_three;
        }else if(type == 4) {
          $scope.where = wheres.msg_four;
        }
      }

      var loadMissing = function(type){
        if(type == 1){
          $scope.miss = missing.msg_one;
        }else if(type == 2){
          $scope.miss = missing.msg_two;
        }else if(type == 3){
          $scope.miss = missing.msg_three;
        }else if(type == 4) {
          $scope.miss = missing.msg_four;
        }
      }

      var loadTips = function(type){
        if(type == 1){
          $scope.tip = tips.msg_one;
        }else if(type == 2){
          $scope.tip = tips.msg_two;
        }else if(type == 3){
          $scope.tip =  tips.msg_three;
        }else if(type == 4) {
          $scope.tip = tips.msg_four;
        } 
      }

      var loadColor = function(type){      
        if(type == 1){
          $scope.colors = colors.color1;
        }else if(type == 2){
          $scope.colors = colors.color2;
        }else if(type == 3){
          $scope.colors = colors.color3;
        }else if(type == 4) {
          $scope.colors = colors.color4;
        }
        console.log("colors", $scope.colors);
      }

      var loadHomes = function(type){      
        if(type == 1){
          $scope.home = homes.home1;
        }else if(type == 2){
          $scope.home = homes.home2;
        }else if(type == 3){
          $scope.home = homes.home3;
        }else if(type == 4) {
          $scope.home = homes.home4;
        }
        console.log("home", $scope.home);
      }

      var inicialize = function () {
        var type = $stateParams.id;        
        loadWhere(type);
        loadMissing(type);
        loadTips(type);
        loadColor(type);
        loadHomes(type);
      }();
    }
}());

/**
 * Service for business logic
 *
 * @author Nelson David Padilla H. phndavid@gmail.com
 * @since 20-01-2017
 *
 */
(function(){
  'use strict';
  angular.module('AdsbApp')
  .service('FeedBackService',FeedBackService)
  FeedBackService.$inject = ['RestService'];
  function FeedBackService(RestService){
    var getWhere = function () {
        return {          
          msg_one: "La empresa cuenta con un importante avance de implementación y cumplimiento de requisitos del sistema de gestión 25%, debemos identificar cuáles son las brechas y empezar a trabajar.",
          msg_two: "Las noticias son muy buenas un 50% de avance de implementación es una buena noticia. Debemos saber cuáles son las brechas y desarrollar un plan de acción que esperamos pronto muestre sus resultados, como hasta ahora lo ha hecho  la empresa",
          msg_three: "Muy buenas noticias, el resultado del esfuerzo y el trabajo nos sitúan en un 75% de avance, nos hace falta poco, una lista de chequeo de lo que tenemos por mejorar y cerrar para el sistema de gestión implementado. ",
          msg_four: "Un excelente resultado, 100% de implementación del sistema de gestión. ",
        }
    }
    var getMissing = function(){
      return {
        msg_one:"Lo más importante por implementar cuando contamos con porcentaje de avance y hemos iniciado con la implementación es tener claro el plan de acción y aquí te lo ayudamos a construir.",
        msg_two:"Es importante cuando tienes un % de avance importante, revisar lo que nos está haciendo falta, que tipo de desarrollo y acompañamiento requiere el personal y que ajustes debemos hacer para avanzar en el camino adecuado, la planeación y la gestión de lo que hace falta es de vital importancia para asegurar el éxito del sistema de gestión. ",
        msg_three:"Prepararse para que el sistema de gestión se consolide como una herramienta de gestión y organización, y para las auditorías internas que están incluidas en nuestros paquetes de servicios",
        msg_four:"Falta demostrar mediante una auditoria presencial la conformidad del sistema de gestión, nosotros la programamos y la hacemos en la empresa, presentamos el informe con el objeto de identificar las posibles desviaciones o mejoras del sistema de gestión. ",
      }
    }
    var getTips = function(){
      return {
        msg_one:[
            {tip: "La socialización de los avances de implementación son importantes para todo el equipo.", icon:"nicono1.png"},
            {tip: "El equipo debe conocer los componentes actuales de los sistemas de gestión.",icon:"nicono2.png"},
            {tip: "Sobre la gestión documental se deben mantener los documentos, los registros y las evidencias, recuerda que pueden ser visuales, electrónicas o físicas, lo que sea más óptimo para cada uno de los procesos.",icon:"nicono3.png"}
          ],          
        msg_two:[
            {tip: "Cómo está la matriz de riesgo de la empresa?", icon:"micono1.png"},
            {tip: "Estamos trabajando sobre los objetivos, política del sistema de gestión?",icon:"micono2.png"},
            {tip: "Es importante la formación en sistema de gestión para los colaboradores, nosotros te decimos como.",icon:"micono3.png"}
          ],                
        msg_three:[
            {tip: "El personal, el cliente, la gestión, el clima deben estar en permanente evaluación y proceso de mejoramiento.",icon:"aicono1.png"},
            {tip: "La gestión documental debe ser una excelente herramienta de trabajo.", icon:"aicono2.png"},
            {tip: "El Ciclo PHVA aunque muy utilizado aplica para las tareas cotidianas y las más estratégicas no lo olvidemos.", icon:"aicono3.png"}         
        ],
        msg_four:[
            {tip: "Luego de las auditorías internas, preparar la gestión de los respectivos planes de acción.",icon:"vicono1.png"},
            {tip: "Del cierre del ciclo PHVA depende el avance y la consolidación del sistema de gestión.",icon:"vicono2.png"},
            {tip: "La validación con el cliente nunca se detiene.",icon:"vicono3.png"}
        ]        
      } 
    }
    var getHomes = function(){
      return {
        home1: [{icon1:"casa_naranja_1.jpg", icon2: "gris_2.png", icon3: "gris_3.png", icon4: "gris_4.png"}],
        home2: [{icon1:"casa_violeta_1.png", icon2: "casa_violeta_2.png", icon3: "gris_3.png", icon4: "gris_4.png"}],
        home3: [{icon1:"casa_azul_1.jpg", icon2: "casa_azul_2.jpg", icon3: "casa_azul_3.jpg", icon4: "gris_4.png"}],
        home4: [{icon1:"casa_verde_1.jpg", icon2: "casa_verde_2.jpg", icon3: "casa_verde_3.jpg", icon4: "casa_verde_4.jpg"}],
      }
    }
    return {
        getWhere: getWhere,
        getMissing: getMissing,
        getTips: getTips,
        getHomes: getHomes
    }
  }
}());

(function () {
    "use strict";

    angular
      .module("AdsbApp")
    /* Directives */

        // change page title
        .directive('updateTitle', [
            '$rootScope',
            function ($rootScope) {
                return {
                    link: function (scope, element) {
                        var listener = function (event, toState, toParams, fromState, fromParams) {
                            var title = 'Yukon Admin';
                            if (toState.page_title) {
                                title = toState.page_title;
                            }
                            if ($rootScope.appVer) {
                                element.text(title + ' (' + $rootScope.appVer + ')');
                            } else {
                                element.text(title);
                            }
                        };
                        $rootScope.$on('$stateChangeStart', listener);
                    }
                }
            }
        ])
        // page preloader
        .directive('pageLoader', [
            '$timeout',
            function ($timeout) {
                return {
                    restrict: 'AE',
                    template: '<div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div>',
                    link: function (scope, el, attrs) {
                        el.addClass('pageLoader hide');
                        scope.$on('$stateChangeStart', function (event) {
                            el.toggleClass('hide animate');
                        });
                        scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
                            event.targetScope.$watch('$viewContentLoaded', function () {
                                $timeout(function () {
                                    el.toggleClass('hide animate')
                                }, 600);
                            })
                        });
                    }
                };
            }
        ])
        // show/hide side menu
        .directive('menuToggle', [
            '$rootScope',
            '$cookieStore',
            '$window',
            '$timeout',
            function ($rootScope, $cookieStore, $window, $timeout) {
                return {
                    restrict: 'E',
                    template: '<span class="menu_toggle" ng-click="toggleSidebar()"><span class="icon_menu_toggle" ><i class="arrow_carrot-2left" ng-class="sideNavCollapsed ? \'hide\' : \'\'"></i><i class="arrow_carrot-2right" ng-class="sideNavCollapsed ? \'\' : \'hide\'"></i></span></span>',
                    link: function (scope, el, attrs) {
                        var mobileView = 992;
                        $rootScope.getWidth = function () {
                            return window.innerWidth;
                        };
                        $rootScope.$watch($rootScope.getWidth, function (newValue, oldValue) {
                            if (newValue >= mobileView) {
                                if (angular.isDefined($cookieStore.get('sideNavCollapsed'))) {
                                    if ($cookieStore.get('sideNavCollapsed') == false) {
                                        $rootScope.sideNavCollapsed = false;
                                    } else {
                                        $rootScope.sideNavCollapsed = true;
                                    }
                                } else {
                                    $rootScope.sideNavCollapsed = false;
                                }
                            } else {
                                $rootScope.sideNavCollapsed = true;
                            }
                            $timeout(function () {
                                $(window).resize();
                            });
                        });
                        scope.toggleSidebar = function () {
                            $rootScope.sideNavCollapsed = !$rootScope.sideNavCollapsed;
                            $cookieStore.put('sideNavCollapsed', $rootScope.sideNavCollapsed);
                            if (!$rootScope.fixedLayout) {
                                if (window.innerWidth > 991) {
                                    $timeout(function () {
                                        $(window).resize();
                                    });
                                }
                            }
                            if (!$rootScope.sideNavCollapsed && !$rootScope.topMenuAct) {
                                $rootScope.createScrollbar();
                            } else {
                                $rootScope.destroyScrollbar();
                            }
                        };
                    }
                };
            }
        ])
        // update datatables fixedHeader position
        .directive('updateFixedHeaders', function ($window) {
            return function (scope, element) {
                var w = angular.element($window);
                scope.getElDimensions = function () {
                    return {
                        'w': element.width(),
                        'h': element.height()
                    };
                };
                scope.$watch(scope.getElDimensions, function (newValue, oldValue) {
                    if (typeof oFH != 'undefined') {
                        oFH._fnUpdateClones(true);
                        oFH._fnUpdatePositions();
                    }
                }, true);
                w.bind('resize', function () {
                    scope.$apply();
                });
            };
        })
        // ng-repeat after render callback
        .directive('onLastRepeat', function ($timeout) {
            return function (scope, element, attrs) {
                if (scope.$last) {
                    $timeout(function () {
                        scope.$emit('onRepeatLast', element, attrs);
                    })
                }
            };
        })
        // add width/height properities to Image
        .directive('addImageProp', function () {
            return {
                restrict: 'A',
                link: function (scope, elem, attr) {
                    elem.on('load', function () {
                        var w = !scope.isHighDensity() ? $(this).width() : $(this).width() / 2,
                            h = !scope.isHighDensity() ? $(this).height() : $(this).height() / 2;
                        $(this).attr('width', w).attr('height', h);
                    });
                }
            };
        })


})();
(function () {
    "use strict";

    angular.module("AdsbApp")
           .controller("SideMenuController", SideMenuController);


    SideMenuController.$inject = ["$rootScope", "$scope", "$state", "$stateParams", "$timeout"];

    function SideMenuController($rootScope, $scope, $state, $stateParams, $timeout) {

        $scope.sections = [
            {
                id: 0,
                title: "Aircraft",
                icon: "fa fa-map-o first_level_icon",
                link: "auth.aircraft"
            },
            {
                id: 1,
                title: "Find",
                icon: "fa fa-search first_level_icon",
                link: "auth.aircraft-find"
            }
        ];

        // accordion menu
        $(document).off("click", ".side_menu_expanded #main_menu .has_submenu > a").on("click", ".side_menu_expanded #main_menu .has_submenu > a", function () {
            if ($(this).parent(".has_submenu").hasClass("first_level")) {
                var $this_parent = $(this).parent(".has_submenu"),
                    panel_active = $this_parent.hasClass("section_active");

                if (!panel_active) {
                    $this_parent.siblings().removeClass("section_active").children("ul").slideUp("200");
                    $this_parent.addClass("section_active").children("ul").slideDown("200");
                } else {
                    $this_parent.removeClass("section_active").children("ul").slideUp("200");
                }
            } else {
                var $submenu_parent = $(this).parent(".has_submenu"),
                    submenu_active = $submenu_parent.hasClass("submenu_active");

                if (!submenu_active) {
                    $submenu_parent.siblings().removeClass("submenu_active").children("ul").slideUp("200");
                    $submenu_parent.addClass("submenu_active").children("ul").slideDown("200");
                } else {
                    $submenu_parent.removeClass("submenu_active").children("ul").slideUp("200");
                }
            }
        });

        $rootScope.createScrollbar = function () {
            $("#main_menu .menu_wrapper").mCustomScrollbar({
                theme: "minimal-dark",
                scrollbarPosition: "outside"
            });
        };

        $rootScope.destroyScrollbar = function () {
            $("#main_menu .menu_wrapper").mCustomScrollbar("destroy");
        };

        $timeout(function () {
            if (!$rootScope.sideNavCollapsed && !$rootScope.topMenuAct) {
                if (!$("#main_menu .has_submenu").hasClass("section_active")) {
                    $("#main_menu .has_submenu .act_nav").closest(".has_submenu").children("a").click();
                } else {
                    $("#main_menu .has_submenu.section_active").children("ul").show();
                }
                // init scrollbar
                $rootScope.createScrollbar();
            }
        });


    }
})();

/**
 * Servicio para el manejo de las ventanas de dialogo
 * 
 * @author demorales13@gmail.com
 * @since 3-dic-2016
 *
 */

(function () {
    'use strict';

    angular.module('AdsbApp')
           .service('DialogService', DialogService);

    DialogService.$inject = ['$mdDialog', '$http'];

    
    function DialogService($mdDialog, $http) {

        // Despliega un confirm popup
        var confirm = function (titulo, mensaje) {

            var confirm = $mdDialog.confirm()
                                   .title(titulo)
                                   .content(mensaje)
                                   .ariaLabel('Confirmación de usuario')
                                   .ok('Si')
                                   .cancel('No')
                                   .hasBackdrop(true);

            return $mdDialog.show(confirm);

        }

        // Despliega un alert popup
        var alert = function (mensaje) {
            $mdDialog.show($mdDialog.alert()
                               .title('')
                               .content(mensaje)
                               .ok('Aceptar')
                               .hasBackdrop(true)
                    );
        }

        return {
            confirm: confirm,
            alert: alert
        }
    }
})();
/**
 * Servicio para interactual con los mapas de google
 *
 * @author demorales13@gmail.com
 * @since 17-dic-2016
 *
 */

(function () {
    "use strict";

    angular
        .module('AdsbApp')
        .service('GoogleMapService', GoogleMapService);

    GoogleMapService.$inject = [];

    function GoogleMapService() {

        var drawPath = function (path) {

            var collection = [];

            for (var i = 1; i < path.length; i++) {
                if (path[i].msgtype == 3 && path[i].latitude != null && path[i].longitude != null && path[i - 1].latitude != null && path[i - 1].longitude != null) {
                    var step = [path[i - 1], path[i]];
                    var color = altitudeColor(path[i].altitude);
                    var draw = drawStep(step, color);
                    collection.push(draw);
                }
            }

            return collection;
        }

        var drawStep = function (step, color) {
            return {
                path: step,
                stroke: {
                    color: color,
                    weight: 3
                },
                geodesic: true,
                visible: true,
                icons: [{
                    icon: {
                        path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW
                    },
                    offset: '25px',
                    repeat: '50px'
                }]
            };
        }

        var drawMarkers = function (type, markers) {
            var collection = [];

            for (var i = 0; i < markers.length; i++) {
                collection.push(createMarker(i, type, markers[i]));
            }

            return collection;
        }

        var createMarker = function (i, type, point) {
            var ret = {
                id: i,
                latitude: point.latitude,
                longitude: point.longitude,
                alert: point.alert,
                icon: iconType(type)
            };

            return ret;
        }

        var iconType = function (type) {

            if (type == "gndspd") {
                return 'content/images/speed.png';
            } else if (type == "vspd") {
                return 'content/images/caution.png';
            } else if (type == "emerg") {
                return 'content/images/flag.png';
            } else if (type == "sqwk") {
                return 'content/images/radiotower.png';
            }

        }

        var altitudeColor = function (altitude) {

            if (altitude >= 0 && altitude <= 499)
                return '#FF0000';
            else if (altitude >= 500 && altitude <= 999)
                return '#FF6600';
            else if (altitude >= 1000 && altitude <= 1999)
                return '#CC9900';
            else if (altitude >= 2000 && altitude <= 2999)
                return '#FFCC00';
            else if (altitude >= 3000 && altitude <= 4999)
                return '#00CC00';
            else if (altitude >= 5000 && altitude <= 7499)
                return '#0033FF';
            else if (altitude >= 7500 && altitude <= 10000)
                return '#9900CC';
        }

        var fitMap = function (map, polylines) {
            var bounds = new google.maps.LatLngBounds();

            var firtsStep = new google.maps.LatLng(polylines[0].path[0].latitude,
                polylines[0].path[0].longitude);
            var lastStep = new google.maps.LatLng(polylines[polylines.length - 1].path[0].latitude,
                polylines[polylines.length - 1].path[0].longitude);
            
            bounds.extend(firtsStep);
            bounds.extend(lastStep);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        }

        return {
            drawPath: drawPath,
            drawMarkers: drawMarkers,
            fitMap: fitMap
        }
    }
} ());

/**
 * Servicio para el manejo de las operaciones de almacenamiento en el storage
 * 
 * @author demorales13@gmail.com
 * @since 3-dic-2016
 *
 */

(function () {
    "use strict";

    angular.module("AdsbApp")
           .service("LocalStorageService", LocalStorageService);

    LocalStorageService.$inject = ["$window"];

    function LocalStorageService($window) {

        // Se selecciona el servicio de almacenamiento
        var store = $window.localStorage;
        var prefix = "AdsbApp_";

        // Adiciona una nueva variable
        var add = function (key, value) {
            value = angular.toJson(value);
            store.setItem(key, value);
        }

        // Obtiene una variable
        var get = function (key) {
            var value = store.getItem(key);

            if (value) {
                value = angular.fromJson(value);
            }

            return value;
        }

        // Elimina una variable
        var remove = function (key) {
            store.removeItem(key);
        }

        return {
            add: add,
            get: get,
            remove: remove
        }
    }

})();
/**
 * Servicio para el manejo de las operaciones REST
 *
 * @author demorales13@gmail.com
 * @since 3-dic-2016
 *
 */

(function () {
    'use strict';

    angular
        .module('AdsbApp')
        .service('RestService', RestService);

    RestService.$inject = ['$http', '$q', 'BaseUri'];

    function RestService($http, $q, BaseUri, LoginBaseUri) {

        // Servicio post
        var post = function (path, data) {
            return $q.resolve($http({
                method: 'POST',
                url: BaseUri.url + path,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: data
            }));
        };

        // Servicio get
        var get = function (path) {          
            return $q.resolve($http.get(BaseUri.url + path));
        };

        // Servicio login
        var login = function (path, body) {
            // todo: puede falta la adición de un config
            return $q.resolve($http.post(BaseUri.url + path, body));
        };

        return {
            post: post,
            get: get,
            login: login
        }
    }
})();
